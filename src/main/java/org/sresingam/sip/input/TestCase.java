package org.sresingam.sip.input;

import java.util.LinkedList;

import org.sresingam.sip.pojo.ConversionTestCaseObject;

/** 
 * TestCase interface. Implementation of this interface could be different
 * nature of test cases, like conversion (which accepts 3 inputs) and weight 
 * (which accepts 2 inputs) 
 * @author Fazreil Amreen
 */
public interface TestCase {
	
	public String getDescription();
	
	public LinkedList getTestCaseObjectList();

}

package org.sresingam.sip.input.impl;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import org.sresingam.sip.input.TestCase;
import org.sresingam.sip.input.Reader;
import org.sresingam.sip.input.XmlReader;
import org.sresingam.sip.pojo.ConversionTestCaseObject;
import org.sresingam.sip.pojo.ConversionTestCaseObject.Input;

public class XmlConversionTestCaseReader extends XmlReader implements Reader,TestCase {

	private LinkedList<ConversionTestCaseObject> conversionTestCaseObjects;
	private String description;
	
	/**
	 * This function returns the description of the testcase
	 */
	@Override
	public String getDescription() {
		return description;
	}
	
	/**
	 * This function reads from a given File and retrieves test cases from the xml file. It then populate the testcases into a list for consumption by TestURL
	 * 
	 * @param file
	 * 				xml file consisting testcases
	 */
	@Override
	public void read(File file) {
		Document inputFile;
		Element conversion;
		List<Node> testCases;
		try {
			inputFile = parse(file);
			conversion = inputFile.getRootElement();
			if(!conversion.getName().equals("conversion")) {
				throw(new Exception("This is now conversion input file"));
			}
			this.description = conversion.selectSingleNode("/conversion").valueOf("@type");
			testCases = conversion.selectNodes("/conversion/testCase");
			conversionTestCaseObjects = parseTestCases(testCases);
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
	
	/**
	 * This function forms a LinkedList of testcases in the form of ConversionTestCaseObject. It converts a List of input in Node form into the said LinkedList 
	 * 
	 * @param testCases
	 * 				List of testCases in Node form
	 */
	private LinkedList<ConversionTestCaseObject> parseTestCases(List<Node> testCases){
		ConversionTestCaseObject conversionTestCaseObject;
		conversionTestCaseObjects = new LinkedList<ConversionTestCaseObject>();
		List<Node> testCaseList = testCases;
		for(Node testCase:testCaseList) {
			conversionTestCaseObject = new ConversionTestCaseObject();
			
			String description = testCase.valueOf("@description");
			String expectedOutput = testCase.selectSingleNode("expectedOutput").getText();
			
			Input input = conversionTestCaseObject.new Input();
			Node inputNode = testCase.selectSingleNode("input");
			input.setFrom(inputNode.selectSingleNode("from").getText());
			input.setValue(inputNode.selectSingleNode("value").getText());
			input.setTo(inputNode.selectSingleNode("to").getText());
			
			conversionTestCaseObject.setDescription(description);
			conversionTestCaseObject.setExpectedOutput(expectedOutput);
			conversionTestCaseObject.setInput(input);
			
			conversionTestCaseObjects.add(conversionTestCaseObject);
		}
		
		return conversionTestCaseObjects;
		
	}

	/**
	 * This function returns the LinkedList of testcases
	 */
	@Override
	public LinkedList<ConversionTestCaseObject> getTestCaseObjectList() {
		// TODO Auto-generated method stub
		return conversionTestCaseObjects;
	}


}
